/** @format */

const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const question = (question) => {
  return new Promise((resolve) => {
    rl.question(question, (answer) => {
      resolve(answer);
    });
  });
};

const kalkulasi = async () => {
  console.table(
    [
      { 'Jenis Operasi': 'Penjumlahan' },
      { 'Jenis Operasi': 'Pengurangan' },
      { 'Jenis Operasi': 'Perkalian' },
      { 'Jenis Operasi': 'Pembagian' },
      { 'Jenis Operasi': 'Akar Quadrat' },
      { 'Jenis Operasi': 'Luas Persegi' },
      { 'Jenis Operasi': 'Volume Kubus' },
      { 'Jenis Operasi': 'Volume Tabung' },
    ],
    ['Jenis Operasi']
  );
  const operasi = await question('Pilih Operasi Yang diinginkan: ');
  let angka1, angka2, sisi, rusuk, r, t, result;

  // Ternary Operator.
  // operasi == 'Penjumlahan'
  //   ? (result = +angka1 + +angka2)
  //   : operasi == 'Pengurangan'
  //   ? (result = +angka1 - +angka2)
  //   : operasi == 'Perkalian'
  //   ? (result = +angka1 * +angka2)
  //   : operasi == 'Pembagian'
  //   ? (result = +angka1 / +angka2)
  //   : operasi == 'Akar Quadrat'
  //   ? (result = +angka1 / +angka2)
  //   : operasi == 'Luas Persegi'
  //   ? (result = angka1 * angka1)
  //   : (result = 'Operasi Tidak Dikenali');

  // If Else If Else
  if (operasi == '0') {
    angka1 = await question('Masukkan Angka Pertama: ');
    angka2 = await question('Masukkan Angka Kedua: ');
    result = `Hasil Penjumlahan dari ${angka1} + ${angka2} adalah ${+angka1 + +angka2}`;
  } else if (operasi == '1') {
    angka1 = await question('Masukkan Angka Pertama: ');
    angka2 = await question('Masukkan Angka Kedua: ');
    result = `Hasil Pengurangan dari ${angka1} - ${angka2} adalah ${angka1 - angka2}`;
  } else if (operasi == '2') {
    angka1 = await question('Masukkan Angka Pertama: ');
    angka2 = await question('Masukkan Angka Kedua: ');
    result = `Hasil Perkalian dari ${angka1} * ${angka2} adalah ${angka1 * angka2}`;
  } else if (operasi == '3') {
    angka1 = await question('Masukkan Angka Pertama: ');
    angka2 = await question('Masukkan Angka Kedua: ');
    result = `Hasil Pembagian dari ${angka1} / ${angka2} adalah ${angka1 / angka2}`;
  } else if (operasi == '4') {
    angka1 = await question('Masukkan Angka yang Ingin di Kuadratkan: ');
    result = `Akar Kuadrat dari ${angka1} adalah ${angka1 ** 2}`;
  } else if (operasi == '5') {
    sisi = await question('Masukkan Sisi Persegi: ');
    result = `Luas Persegi dengan sisi persegi ${sisi} adalah ${sisi * sisi}cm²`;
  } else if (operasi == '6') {
    rusuk = await question('Masukkan Rusuk Kubus: ');
    result = `Volume Kubus dengan rusuk kubus ${rusuk} adalah ${rusuk * rusuk * rusuk}cm³`;
  } else if (operasi == '7') {
    r = await question('Masukkan Jari-Jari alas: ');
    t = await question('Masukkan Tinggi: ');
    result = `Volume Tabung dengan Jari-Jari alas ${r} dan Tinggi ${t} adalah ${3.14 * r * r * t}cm³`;
  } else {
    result = 'Operasi Tidak Dikenali';
  }

  console.log(result);
  rl.close();
};

kalkulasi();
